import axios from 'axios';
import { GET_POKEMON, DETAIL_POKEMON, SET_MYPOKEMON, AUTH_ERROR, UPDATE_MYPOKEMON } from './type';
import { endpoints } from './api';

export const getPokemon = () => dispatch => {
    axios.get(endpoints.getPokemon)
            .then(res => {
                dispatch({
                    type: GET_POKEMON,
                    payload: res.data.results,
                })
            })
            .catch(err => {
                dispatch({
                    type: AUTH_ERROR,
                    error: err.response.data.message
                })
            });
}

export const getDetailPokemon = (url) => dispatch => {
    axios.get(endpoints.detailPokemon(url))
        .then(res => {
            dispatch({
                type: DETAIL_POKEMON,
                payload: res.data
            })
        })
        .catch(err => {
            dispatch({
                type: AUTH_ERROR,
                error: err.response.data.message
            })
        });
}

export const setPokemon = (payload) => dispatch => {
    dispatch({
        type: SET_MYPOKEMON,
        payload
    })
}

export const updatePokemon = (payload) => dispatch => {
    dispatch({
        type: UPDATE_MYPOKEMON,
        payload
    })
}