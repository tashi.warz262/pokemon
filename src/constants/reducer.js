import { GET_POKEMON, DETAIL_POKEMON, SET_MYPOKEMON, AUTH_ERROR, UPDATE_MYPOKEMON } from "./type";

const initState = {
    Pokemon: null,
    DetailPokemon: null,
    MyPokemon: [],
    Error: null
}

const rootReducer = (state = initState, action) => {
    const { type, payload, error } = action;
    switch(type) {
      case 'reset':
        return initState;
      case GET_POKEMON:
        return {
          ...state,
          Pokemon: payload,
        };
      case DETAIL_POKEMON:
        return {
          ...state,
          DetailPokemon: payload
        }
      case SET_MYPOKEMON:
        return {
          ...state,
          MyPokemon: payload
        }
      case UPDATE_MYPOKEMON:
        return {
          ...state,
          MyPokemon: [...state.MyPokemon, payload]
        }
      case AUTH_ERROR:
        return {
          ...state,
          Error: error
        }
      default:
        return state;
    }
}

export default rootReducer;