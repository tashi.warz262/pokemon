import Home from './Home';
import { withRouter } from 'next/router';
import { connect } from 'react-redux';
import * as actions from '../../constants/actions';
import { bindActionCreators } from 'redux';

const Routered = withRouter(Home);
const mapStateToProps = ({ Pokemon, DetailPokemon, MyPokemon, Error }) => ({ Pokemon, DetailPokemon, MyPokemon, Error });
const mapDispatchToProps = (dispatch) => ({
  actions: bindActionCreators(actions, dispatch)
});
export default connect(mapStateToProps, mapDispatchToProps)(Routered);