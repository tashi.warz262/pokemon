import React from 'react';
import { css } from '@emotion/css';
import { isNil } from 'ramda';
import { Header, Footer } from 'antd/lib/layout/layout';
import { Button, Table, Tooltip } from 'antd';
import { get } from 'lodash';

export default class Home extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      halamanAwal: true,
      modalDetail: false,
      namaPokemon: null,
      modalCatch: false,
      modalTryAgain: false,
      modalErrorName: false,
      modalSuccess: false,
      idPokemon: null,
    }
  }
  componentDidMount () {
    const { actions } = this.props;
    actions.getPokemon();
    if(sessionStorage.getItem("MyPokemon") !== null) this._setPokemon();
  }

  componentDidUpdate (prevprops){
    const { DetailPokemon } = this.props;
    if(prevprops.DetailPokemon !== DetailPokemon) this._activeModalDetail();
  }

  _activeModalDetail = () => {
    this.setState({ modalDetail: true });
  }

  _closeModalErrorName = () => {
    this.setState({ modalErrorName: false });
  }

  _success = () => {
    const { MyPokemon } = this.props;
    if(!isNil(MyPokemon) && MyPokemon !== undefined) {
      let payload = { name: this.state.namaPokemon, idPokemon: this.state.idPokemon };
      this.props.actions.updatePokemon(payload);
    }
    else if(isNil(MyPokemon) || MyPokemon === undefined) {
      let Poke = { name: this.state.namaPokemon, idPokemon: this.state.idPokemon };
      let payload = [ Poke ];
      this.props.actions.setPokemon(payload);
    }
    this.setState({ modalSuccess: true, namaPokemon: null, idPokemon: null, modalCatch: false });
  }

  _setValue = (e) => {
    this.setState({ namaPokemon: e.target.value });
  }

  _updateMyPokemon = () => {
    const { MyPokemon } = this.props;
    const { namaPokemon } = this.state;
    let same = 0;
    if(!isNil(MyPokemon) && MyPokemon !== undefined && MyPokemon.length>0){
      for(let i=0;i<MyPokemon.length;i++){
        if(get(MyPokemon, `${i}.name`) === namaPokemon) {
          same += 1;
        }
      }
      if(same>0){
        this.setState({ modalErrorName: true });
      }
      else this._success();
    }
    else if(isNil(MyPokemon) || MyPokemon === undefined) {
      this._success();
    }
  }

  _setPokemon = () => {
    let payload = JSON.parse(sessionStorage.getItem("MyPokemon"));
    this.props.actions.setPokemon(payload);
  }

  _catchPokemon = (nama) => {
    const value = Math.floor((Math.random()*2) + 1);
    if(value === 1) this.setState({ modalCatch: true, idPokemon: nama });
    else this.setState({ modalTryAgain: true });
  }

  _renderPokmeon = () => {
    const { Pokemon } = this.props;
    if(!isNil(Pokemon) && Pokemon !== undefined && Pokemon.length !== 0){
      return Pokemon.map((data, index) => {
        return (
          <div key={index} onClick={this._getDetailPokemon.bind(this, data)} className={css`
            width: 220px;
            height: 240px;
            box-shadow: rgba(0,0,0,0.35) 0px 5px 15px;
            border-radius: 7px;
            background-color: white;
            padding: 20px !important;
            margin: 0 0 50px 50px;
            display: inline-flex;
            cursor: pointer;
            &:hover {
              transform: translateY(-10px);
            }
            @media only screen and (max-width: 470px) {
              margin: 0 0 30px 20px;
            }
            @media only screen and (max-width: 677px) {
              margin: 0 0 30px 20px;
            }
          `}>
            <div>
              <img rel="icon" src={'/'+data.name+'.png'} alt="logo" className={css`
                width: 180px !important;
                height: 130px !important;
              `}/>
              <h4 className={css`
                text-transform: uppercase;
              `}>{data.name}</h4>
              <h4>Owned: {this._renderOwned(data.name)}</h4>
            </div>
          </div>
        )
      })
    }
  }

  _renderDetail = () => {
    const { DetailPokemon } = this.props;
    return(
      <div>
        <div className={css`
          text-align: center;
          background-color: #D35400;
          padding: 20px 30px;
        `}>
          <div className={css`
            display: flex;
          `}>
            <h4 className={css`
              text-transform: uppercase;
              font-size: 18px;
              font-weight: 700;
              font-family: Gil Sans;
              width: 100%;
              text-align: left;
            `}>{DetailPokemon.name}</h4>
            <h4 className={css`
              font-size: 18px;
              font-weight: 700;
              width: 100%;
              text-align: right;
              font-family: Gil Sans;
            `}>Types: {this._renderTypes(DetailPokemon.types)}</h4>
          </div>
          <div className={css`
            margin: auto;
            background-color: white;
            width: 300px;
            border: solid;
            @media only screen and (max-width: 470px) {
              max-width: 250px;
            }
          `}>
            <img rel="icon" src={'/'+DetailPokemon.name+'.png'} alt="logo" className={css`
              max-width: 300px;
              max-height: 220px;
              min-height: 220px;
              @media only screen and (max-width: 470px) {
                max-width: 250px;
              }
            `}/>
          </div>
          <div>
            <h4 className={css`
              margin-top: 10px;
              text-transform: uppercase;
              text-align: left;
              font-size: 16px;
              font-weight: 700;
              font-family: Gil Sans;
              width: 100%;
            `}>Abilities:</h4>
            {this._renderAbilities(DetailPokemon.abilities)}
          </div>
        </div>
        <Button onClick={this._catchPokemon.bind(this, DetailPokemon.name)} className={css`
          margin-top: 10px;
          border: solid;
          border-radius: 10%;
          background-color: red;
          font-weight: 700;
          font-color: white;
        `}>CATCH</Button>
      </div>
    )
  }

  _renderOwned = (name) => {
    const { MyPokemon } = this.props;
    let total = 0
    if(!isNil(MyPokemon) && MyPokemon.length !== 0){
      for(let i=0;i<MyPokemon.length;i++){
        if(get(MyPokemon, `${i}.idPokemon`) === name){
          total += 1;
        }
      }
      return total;
    }
    else return total;
  }

  _renderTypes = (types) => {
    let total = types.length;
    if(!isNil(types) && total>0){
      return types.map(data => {
        return (
          <img src={'/'+data.type.name+'.png'}  rel="icon" alt="logo" className={css`
            width: 25px;
            height: 25px;
          `}/>
        )
      })
    }
  }

  _renderAbilities = (abilities) => {
    let total = abilities.length;
    if(!isNil(abilities) && total>0){
      return abilities.map(data => {
        return (
          <div>
            <h4 className={css`
              margin-left: 10px;
              text-transform: uppercase;
              text-align: left;
              font-size: 14px;
              font-weight: 600;
              font-family: Gil Sans;
              width: 100%;
            `}>* {data.ability.name}</h4>
          </div>
        )
      })
    }
  }

  _closeModalTryAgain = () => {
    this.setState({ modalTryAgain: false });
  }

  _closeModalSuccess = () => {
    sessionStorage.setItem("MyPokemon", JSON.stringify(this.props.MyPokemon));
    this.setState({ modalSuccess: false, modalDetail: false });
  }

  _closeModalDetail = () => {
    this.setState({ modalDetail: false });
  }

  _activePokemonGround = () => {
    this.setState({ halamanAwal: true });
  }

  _activeMyPokemon = () => {
    this.setState({ halamanAwal: false });
  }

  _getDetailPokemon = (data) => {
    this.props.actions.getDetailPokemon(data.url);
  }

  _removePokemon = (record) => {
    const data = this.props.MyPokemon.filter(item => item.name !== record.name).map(({name, idPokemon }) => ({ name, idPokemon  }));
    this.props.actions.setPokemon(data);
    sessionStorage.setItem("MyPokemon", JSON.stringify(data));
  }

  _renderAksi = (record) => {
    return (
      <div>
        <Tooltip title="Remove" placement='bottom'>
          <Button onClick={this._removePokemon.bind(this, record)}>
            <img src='/trash.png' rel="icon" alt='logo' className={css`
              width: 25px;
              height: 25px;
            `}/>
          </Button>
        </Tooltip>
      </div>
    )
  }

  render(){
    const { halamanAwal, modalDetail, modalCatch, modalTryAgain, namaPokemon, modalErrorName, modalSuccess } = this.state;
    const { MyPokemon } = this.props;
    const columns = [
      {
        title: 'Nickname Pokemon',
        dataIndex: ['name'],
        key:'name',
        showSorterTooltip: false,
        width: 100,
      },
      {
        title: 'Pokemon',
        dataIndex: ['idPokemon'],
        key: 'idPokemon',
        showSorterTooltip:false,
        width: 150,
      },
      {
        title: <p className="titleAksi">Aksi</p>,
        key: 'aksi',
        width: 40,
        render: this._renderAksi
      },
  ];
    return(
      <div className={css`
        padding: 0;
        position: sticky;
      `}>
        {modalCatch && <div className={css`
          background-color: rgba(0, 0, 0, 0.5);
          position: absolute;
          width: 100%;
          height: 100%;
          z-index: 1500;
        `}>
          <div className={css`
            position:fixed;
            width: 100%;
          `}>
            <div className={css`
              margin: auto;
              width: 300px;
              max-height: 180px;
              height: 100%;
              margin-top: 250px;
              box-shadow: rgba(0,0,0,0.35) 0px 5px 15px;
              border-radius: 7px;
              background-color: white;
              padding: 20px !important;
              text-align: center;
            `}>
              <h4>Enter the nickname to the new Pokemon:</h4>
              <input placeholder="Nickname" name="name" type="text" value={namaPokemon} onChange={this._setValue}/>
              <Button onClick={this._updateMyPokemon} className={css`
                border: solid;
                border-radius: 10%;
                background-color: yellow;
                font-weight: 700;
                font-color: white;
                margin-left: 10px;
              `}>Done</Button>
            </div>
          </div>
        </div>}
        {modalTryAgain && <div className={css`
          background-color: rgba(0, 0, 0, 0.5);
          position: absolute;
          width: 100%;
          height: 100%;
          z-index: 1500;
        `}>
          <div className={css`
            position:fixed;
            width: 100%;
          `}>
            <div className={css`
              margin: auto;
              width: 300px;
              max-height: 180px;
              height: 100%;
              margin-top: 250px;
              box-shadow: rgba(0,0,0,0.35) 0px 5px 15px;
              border-radius: 7px;
              background-color: white;
              padding: 20px !important;
              text-align: center;
            `}>
              <h4>Sorry, you haven't got this pokemon. Try again later!</h4>
              <Button onClick={this._closeModalTryAgain} className={css`
                border: solid;
                border-radius: 10%;
                background-color: green;
                font-weight: 700;
                font-color: white;
                margin-left: 10px;
              `}>Ok</Button>
            </div>
          </div>
        </div>}
        {modalErrorName && <div className={css`
          background-color: rgba(0, 0, 0, 0.5);
          position: absolute;
          width: 100%;
          height: 100%;
          z-index: 1500;
        `}>
          <div className={css`
            position:fixed;
            width: 100%;
          `}>
            <div className={css`
              margin: auto;
              width: 340px;
              max-height: 200px;
              height: 100%;
              margin-top: 250px;
              box-shadow: rgba(0,0,0,0.35) 0px 5px 15px;
              border-radius: 7px;
              background-color: white;
              padding: 20px !important;
              text-align: center;
            `}>
              <h4>Sorry, your nickname input has been already use. Please input another nickname</h4>
              <Button onClick={this._closeModalErrorName} className={css`
                border: solid;
                border-radius: 10%;
                background-color: green;
                font-weight: 700;
                font-color: white;
                margin-left: 10px;
              `}>Ok</Button>
            </div>
          </div>
        </div>}
        {modalSuccess && <div className={css`
          background-color: rgba(0, 0, 0, 0.5);
          position: absolute;
          width: 100%;
          height: 100%;
          z-index: 1500;
        `}>
          <div className={css`
            position:fixed;
            width: 100%;
          `}>
            <div className={css`
              margin: auto;
              width: 300px;
              max-height: 180px;
              height: 100%;
              margin-top: 250px;
              box-shadow: rgba(0,0,0,0.35) 0px 5px 15px;
              border-radius: 7px;
              background-color: white;
              padding: 20px !important;
              text-align: center;
            `}>
              <h4>Congratulations you have caught them success!</h4>
              <Button onClick={this._closeModalSuccess} className={css`
                border: solid;
                border-radius: 10%;
                background-color: green;
                font-weight: 700;
                font-color: white;
                margin-left: 10px;
              `}>Ok</Button>
            </div>
          </div>
        </div>}
        {modalDetail && <div className={css`
          background-color: rgba(0, 0, 0, 0.5);
          position: absolute;
          width: 100%;
          height: 100%;
          z-index: 1300;
        `}>
          <div className={css`
            position:fixed;
            width: 100%;
          `}>
            <div className={css`
              margin: auto;
              width: 25px;
              height: 25px;
              padding-left: 420px;
              margin-top: 130px;
              text-align: center;
              display: flex;
              @media only screen and (max-width: 470px) {
                padding-left: 370px;
              }
            `}>
              <h4 onClick={this._closeModalDetail} className={css`
                color: white;
                cursor: pointer;
                padding-top: -5px !important;
              `}>x</h4>
            </div>
            <div className={css`
              margin: auto;
              width: 400px;
              max-height: 480px;
              height: 100%;
              margin-top: 10px;
              box-shadow: rgba(0,0,0,0.35) 0px 5px 15px;
              border-radius: 7px;
              background-color: yellow;
              padding: 20px !important;
              text-align: center;
              @media only screen and (max-width: 470px) {
                max-width: 340px;
              }
            `}>
                {this._renderDetail()}
            </div>
          </div>
        </div>}
        <Header className={css`
          display: flex;
          justify-content: center;
          text-align: center;
          position: sticky;
          top: 0;
          width: 100%;
          height: 100%;
          padding-top: 10px;
          padding-bottom: 20px;
          background-color: blue;
          border-bottom-left-radius: 45%;
          border-bottom-right-radius: 45%;
          z-index: 1000;
        `}>
          <img src='/pokeball.png'  rel="icon" alt="logo" className={css`
            width: 60px;
            height: 60px;
            margin-top: 10px;
            @media only screen and (max-width: 470px) {
              width: 20px;
              height: 20px;
            }
          `} />
          <h3 className={css`
            margin: 0;
            line-height: 1.15;
            font-size: 4rem;
            font-style: italic;
            color: yellow;
            @media only screen and (max-width: 470px) {
              font-size: 26px;
              padding-top: 10px;
            }
            @media only screen and (max-width: 677px) {
              font-size: 26px;
              padding-top: 10px;
            }
          `}>POKÉMON GROUND</h3>
        </Header>
        <main className={css`
          min-height: 80vh;
          padding: 0;
          flex: 1;
          display: flex;
          flex-direction: column;
          justify-content: center;
          align-items: center;
        `}>
          <div className={css`
            display: flex;
            justify-content: center;
            align-items: center;
            margin-bottom: 20px;
            margin-top: 20px;
          `}>
            <h4 onClick={this._activePokemonGround} className={css`
              font-size: 24px;
              margin-right: 10px;
              padding: 0 60px 20px;
              border-bottom: solid;
              cursor: pointer;
              &:hover {
                color: #19E121;
              }
              @media only screen and (max-width: 470px) {
                font-size: 16px;
                padding: 0 20px 10px;
                border-bottom: solid;
              }
              @media only screen and (max-width: 677px) {
                font-size: 18px;
                padding: 0 40px 20px;
                border-bottom: solid;
              }
            `}>Pokémon in Ground</h4>
            <h4  onClick={this._activeMyPokemon} className={css`
              font-size: 24px;
              margin-right: 10px;
              padding: 0 60px 20px;
              border-bottom: solid;
              cursor: pointer;
              &:hover {
                color: #19E121;
              }
              @media only screen and (max-width: 470px) {
                font-size: 16px;
                padding: 0 20px 10px;
                border-bottom: solid;
              }
              @media only screen and (max-width: 677px) {
                font-size: 18px;
                padding: 0 40px 20px;
                border-bottom: solid;
              }
            `}>My Pokémon</h4>
          </div>
          {halamanAwal && <div className={css`
            width: 100%;
            justify-content: center;
            align-items: center;
            text-align: center;
          `}>
            {this._renderPokmeon()}
          </div>}
          {!halamanAwal && isNil(MyPokemon) && <div className={css`
            text-align: center;
            @media only screen and (max-width: 470px) {
              max-width : 250px;
            }
            @media only screen and (max-width: 677px) {
              max-width: 350px;
            }
          `}>
            <h1 className={css`
              width= 500;
              @media only screen and (max-width: 470px) {
                font-size: 20px;
              }
              @media only screen and (max-width: 677px) {
                font-size: 24px;
              }
            `}>Sorry, you have not caught any Pokemon at this time.<br/>Please catch your Pokemon first.<br/>Thank You.</h1>
            <img src='/Pokemon.png'  rel="icon" alt="logo" className={css`
              @media only screen and (max-width: 470px) {
                max-width: 250px
              }
              @media only screen and (max-width: 677px) {
                max-width: 350px;
              }
            `}/>
          </div>}
          {!halamanAwal && !isNil(MyPokemon) && <Table columns={columns} dataSource={MyPokemon}/>}
        </main>
        <Footer className={css`
          display: flex;
          flex: 1;
          padding: 2rem 0;
          border-top: 1px solid #eaeaea;
          justify-content: center;
          align-items: center;
          border-top-left-radius: 5%;
          border-top-right-radius: 5%;
          background-color: black;
          color: white;
        `}>
            POKÉMON by Tashi Bhadra Pattra
        </Footer>
      </div>
    )
  }
}